#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "utils.h"
#include "basicf.h"
#include "actions.h"
//preconditions list of action
int precond_list(uint8_t actions[250], int a_pos, uint8_t pre_list[200])
{
	uint8_t k,l,c,d,p,r,m;
	switch(actions[a_pos])
	{
			case take_n:
				/*int take(uint8_t k,uint8_t l,uint8_t c,uint8_t d,uint8_t p,uint8_t state[200], int i, bool j)
				{
				int j1, j2,j3, j4, j5;
				j1=precond(state,i,belong_v, k,l);
				j2=precond(state,i,attached_v,p,l);
				j3=precond(state,i,empty_v,k,0);
				j4=precond(state,i,top_v,c,p);
				j5=precond(state,i,on_v,c,d);*/
				k=actions[a_pos+1];
				l=actions[a_pos+2];
				c=actions[a_pos+3];
				d=actions[a_pos+4];
				p=actions[a_pos+5];
				pre_list[1]=belong_v;
				pre_list[2]= k;
				pre_list[3]= l;
				pre_list[4]= attached_v;
				pre_list[5]= p;
				pre_list[6]= l;
				pre_list[7]= empty_v;
				pre_list[8]= k;
				pre_list[9]= top_v;
				pre_list[10]= c;
				pre_list[11]= p;
				pre_list[12]= on_v;
				pre_list[13]= c;
				pre_list[14]= d;
				return 14;
				break;
			case put_n:
				/*int put(uint8_t k,uint8_t l,uint8_t c,uint8_t d,uint8_t p,uint8_t state[200], int i, bool j)
				{
				int j1, j2,j3, j4;
				j1=precond(state,i,belong_v, k,l);
				j2=precond(state,i,attached_v,p,l);
				j3=precond(state,i,holding_v,k,c);
				j4=precond(state,i,top_v,d,p);*/
				k=actions[a_pos+1];
				l=actions[a_pos+2];
				c=actions[a_pos+3];
				d=actions[a_pos+4];
				p=actions[a_pos+5];
				pre_list[1]=belong_v;
				pre_list[2]= k;
				pre_list[3]= l;
				pre_list[4]= attached_v;
				pre_list[5]= p;
				pre_list[6]= l;
				pre_list[7]= holding_v;
				pre_list[8]= k;
				pre_list[9]= c;
				pre_list[10]=top_v;
				pre_list[11]= d;
				pre_list[12]= p;
				return 12;
				break;
			case move_n:
				/*int move(uint8_t r,uint8_t l,uint8_t m,uint8_t state[200], int i, bool j)
				{
				int j1, j2,j3;
				j1=precond(state,i,adjacent_v, l,m);
				j2=precond(state,i,at_v,r,l);
				j3=precond(state,i,occupied_v,m,0);*/
				r=actions[a_pos+1];
				l=actions[a_pos+2];
				m=actions[a_pos+3];
				pre_list[1]=adjacent_v;
				pre_list[2]= l;
				pre_list[3]= m;
				pre_list[4]= at_v;
				pre_list[5]= r;
				pre_list[6]= l;
				pre_list[7]= occupied_v;
				pre_list[8]= m;
				return 8;
				break;
			case load_n:
				/*int load(uint8_t k,uint8_t l,uint8_t c,uint8_t r,uint8_t state[200], int i, bool j)
				{
				int j1, j2,j3, j4;
				j1=precond(state,i,belong_v, k,l);
				j2=precond(state,i,holding_v,k,c);
				j3=precond(state,i,at_v,r,l);
				j4=precond(state,i,unloaded_v,r,0);*/
				k=actions[a_pos+1];
				l=actions[a_pos+2];
				c=actions[a_pos+3];
				r=actions[a_pos+4];
				pre_list[1]=belong_v;
				pre_list[2]= k;
				pre_list[3]= l;
				pre_list[4]= holding_v;
				pre_list[5]= k;
				pre_list[6]= c;
				pre_list[7]= at_v;
				pre_list[8]= r;
				pre_list[9]= l;
				pre_list[10]= unloaded_v;
				pre_list[11]= r;
				return 11;
				break;
			case unload_n:
				/*int unload(uint8_t k,uint8_t l,uint8_t c,uint8_t r,uint8_t state[200], int i, bool j)
				{
				int j1, j2,j3, j4;
				j1=precond(state,i,belong_v, k,l);
				j2=precond(state,i,at_v,r,l);
				j3=precond(state,i,loaded_v,r,c);
				j4=precond(state,i,empty_v,k,0);*/
				k=actions[a_pos+1];
				l=actions[a_pos+2];
				c=actions[a_pos+3];
				r=actions[a_pos+4];
				pre_list[1]=belong_v;
				pre_list[2]= k;
				pre_list[3]= l;
				pre_list[4]= at_v;
				pre_list[5]= r;
				pre_list[6]= l;
				pre_list[7]= loaded_v;
				pre_list[8]= r;
				pre_list[9]= c;
				pre_list[10]= empty_v;
				pre_list[11]= k;
				return 11;
				break;
	}
}
//ACTION to order the crane to take a cage c, on d, from the top of the pile p
int take(uint8_t k,uint8_t l,uint8_t c,uint8_t d,uint8_t p,uint8_t state[200], int i, bool j)
{
	int j1, j2,j3, j4, j5;
	j1=precond(state,i,belong_v, k,l);
	j2=precond(state,i,attached_v,p,l);
	j3=precond(state,i,empty_v,k,0);
	j4=precond(state,i,top_v,c,p);
	j5=precond(state,i,on_v,c,d);
	if (j1==0 || j2==0 || j3==0|| j4==0|| j5==0)
	{
		if (j==1) printf("ERROR: TAKE Preconditions are not fullfilled\n");
		return 0;
	}
	if (j==0) return 1;
	i=delete_eff(state,i,empty_v,k,0);
	i=delete_eff(state,i,in_v,c,p);
	i=delete_eff(state,i,top_v,c,p);
	i=delete_eff(state,i,on_v,c,d);
	i=holding(state,k,c,i);
	i=top(state,d,p,i);
	return i;
}
//function to put c onto d in pile p from crane k
int put(uint8_t k,uint8_t l,uint8_t c,uint8_t d,uint8_t p,uint8_t state[200], int i, bool j)
{
	int j1, j2,j3, j4;
	j1=precond(state,i,belong_v, k,l);
	j2=precond(state,i,attached_v,p,l);
	j3=precond(state,i,holding_v,k,c);
	j4=precond(state,i,top_v,d,p);
	if (j1==0 || j2==0 || j3==0|| j4==0)
	{
		if (j==1) printf("ERROR: PUT Preconditions are not fullfilled\n");
		return 0;
	}
	if (j==0) return 1;
	i=delete_eff(state,i,holding_v,k,c);
	i=delete_eff(state,i,top_v,d,p);
	i=empty(state,k,i);
	i=in(state,c,p,i);
	i=top(state,c,p,i);
	i=on(state,c,d,i);
	return i;
}
//function, robot r moves from location l to location m
int move(uint8_t r,uint8_t l,uint8_t m,uint8_t state[200], int i, bool j)
{
	int j1, j2,j3;
	j1=precond(state,i,adjacent_v, l,m);
	j2=precond(state,i,at_v,r,l);
	j3=precond(state,i,occupied_v,m,0);
	if(j3==0) j3=1;
	else j3=0;
	if (j1==0 || j2==0 || j3==0)
	{
		if (j==1) printf("ERROR: MOVE Preconditions are not fullfilled\n");
		return 0;
	}
	if (j==0) return 1;
	i=delete_eff(state,i,occupied_v,l,0);
	i=delete_eff(state,i,at_v,r,l);
	i=at(state,r,m,i);
	i=occupied(state,m,i);
	return i;
}
//crane k at location l loads container c onto robot r
int load(uint8_t k,uint8_t l,uint8_t c,uint8_t r,uint8_t state[200], int i, bool j)
{
	int j1, j2,j3, j4;
	j1=precond(state,i,belong_v, k,l);
	j2=precond(state,i,holding_v,k,c);
	j3=precond(state,i,at_v,r,l);
	j4=precond(state,i,unloaded_v,r,0);
	if (j1==0 || j2==0 || j3==0|| j4==0)
	{
		if (j==1) printf("ERROR:LOAD Preconditions are not fullfilled\n");
		return 0;
	}
	if (j==0) return 1;
	i=delete_eff(state,i,holding_v,k,c);
	i=delete_eff(state,i,unloaded_v,r,0);
	i=empty(state,k,i);
	i=loaded(state,r,c,i);
	return i;
}
//crane k at location l takes container c from robot r
int unload(uint8_t k,uint8_t l,uint8_t c,uint8_t r,uint8_t state[200], int i, bool j)
{
	int j1, j2,j3, j4;
	j1=precond(state,i,belong_v, k,l);
	j2=precond(state,i,at_v,r,l);
	j3=precond(state,i,loaded_v,r,c);
	j4=precond(state,i,empty_v,k,0);
	if (j1==0 || j2==0 || j3==0|| j4==0)
	{
		if (j==1) printf("ERROR: UNLOAD Preconditions are not fullfilled\n");
		return 0;
	}
	if (j==0) return 1;
	i=delete_eff(state,i,empty_v,k,0);
	i=delete_eff(state,i,loaded_v,r,c);
	i=holding(state,k,c,i);
	i=unloaded(state,r,i);
}
