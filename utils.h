//first set the names of the arguments
//uint8_t p1, p2, c1, c2, c3, crane1, r1, loc1, loc2;
//p1=1; p2=2; c1=3; c2=4; c3=5; crane1=6; r1=7; loc1=8; loc2=9;
#define p1 1
#define p2 2
#define c1 3
#define c2 4
#define c3 5
#define crane1 6
#define r1 7
#define loc1 8
#define loc2 9
//DEFINE NUMBER OF ELEMENTS
#define NP 2
#define NC 3
#define NCRANE 1
#define NR 1
#define NLOC 2
//then set the names of the atoms
#define adjacent_v 10
#define attached_v 11
#define belong_v 12
#define occupied_v 13
#define at_v 14
#define loaded_v 15
#define unloaded_v 16
#define holding_v 17
#define empty_v 18
#define in_v 19
#define on_v 20
#define top_v 21
//set of numbers asotiated to actions
#define take_n 22
#define put_n 23
#define move_n 24
#define load_n 25
#define unload_n 26
//#define NELEMS(x) (sizeof(x) / sizeof((x)[0]))
//define an infinite value
#define MY_INFINITY 20000
