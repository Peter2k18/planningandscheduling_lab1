#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "exercise4f.h"
#include "utils.h"
#include "basicf.h"
#include "actions.h"
#include "exercise5f.h"
#include "exercise6f.h"

int add_action(uint8_t plan[250], int pi, uint8_t actions[250], int a_pos)
{
	int j;
	int sizeofaction=getsizeofaction(actions[a_pos]);
	for(j=0;j<sizeofaction;j++)
	{
		plan[pi+j]=actions[a_pos+j];
	}
	return pi+sizeofaction;
}
int execute_action(uint8_t state[200],int i,uint8_t actions[250],int a_pos)
{
	uint8_t k, l, c, p, r, d, m;
	switch(actions[a_pos])
	{
		case take_n:
			k=actions[a_pos+1];
			l=actions[a_pos+2];
			c=actions[a_pos+3];
			d=actions[a_pos+4];
			p=actions[a_pos+5];
			i=take(k,l,c,d,p,state,i,1);
			break;
		case put_n:
			k=actions[a_pos+1];
			l=actions[a_pos+2];
			c=actions[a_pos+3];
			d=actions[a_pos+4];
			p=actions[a_pos+5];
			i=put(k,l,c,d,p,state,i,1);
			break;
		case move_n:
			r=actions[a_pos+1];
			l=actions[a_pos+2];
			m=actions[a_pos+3];
			i=move(r,l,m,state,i,1);
			break;
		case load_n:
			k=actions[a_pos+1];
			l=actions[a_pos+2];
			c=actions[a_pos+3];
			r=actions[a_pos+4];
			i=load(k,l,c,r,state,i,1);
			break;
		case unload_n:
			k=actions[a_pos+1];
			l=actions[a_pos+2];
			c=actions[a_pos+3];
			r=actions[a_pos+4];
			i=unload(k,l,c,r,state,i,1);
			break;
	}
}
int forward_search(uint8_t plan[250], int pi, uint8_t state[200],int i,uint8_t goal[200],int g, uint8_t prev_state[200], int l_prev_state)
{
	uint8_t gamma_state[200], options[250], new_plan[250];
	int comp1=1, comp2=2;
	printf("\n\nActual plan is:\n");
	print_actions(plan, pi);
	int gs_len,x, options_pos[100], prop_pos[100];
	int delt=delta0_sg(state, i, goal, g);
	printf("distance to goal is:%d\n", delt);
	if (delt==0) return pi;
	else
	{
		int opt_len=applicableactions(state, i, options);
		printf("Initial options are:\n");
		print_actions(options, opt_len);
		int min;
		while(opt_len>1)
		{
			//chosen_act
			int n_opt=count_actions(options,opt_len,options_pos);
			printf("number of options is: %d\n", n_opt);
			for(x=1;x<=i;x++)
			{
				gamma_state[x]=state[x];
			}
			gs_len=i;
			gs_len=execute_action(gamma_state, gs_len, options, options_pos[1]);
			//printf("not segmentated yet, after execute action\n");
			if(l_prev_state>0)
			{
				comp1=compare_states(gamma_state,gs_len, prev_state,l_prev_state);
				comp2=count_propositions(prev_state, l_prev_state, prop_pos);
			}
		//printf("is the goal reached? a=%d, b=%d\n", a, b);
			if (comp1==comp2) min=MY_INFINITY*100;
			else min=delta0_sg(gamma_state, gs_len, goal, g);
			int min_opt=options_pos[1];
			int min2,j;
			//printf("not segmentated after first delta0\n");
			for(j=2;j<=n_opt;j++)
			{
				for(x=1;x<=i;x++)
				{
					gamma_state[x]=state[x];
				}
				gs_len=i;
				gs_len=execute_action(gamma_state, gs_len, options, options_pos[j]);
				//printf("state when executing action is:\n");
				//print_state(gamma_state, gs_len);
				//printf("not segmentated before delta j=%d\n", j);
				//uint8_t aux_prep[5];
				//aux_prep[1]=occupied_v;
				//aux_prep[2]=loc2;
				//min2=delta0_sp(gamma_state, gs_len, aux_prep, 2);
				if(l_prev_state>0)
				{
					comp1=compare_states(gamma_state,gs_len, prev_state,l_prev_state);
					comp2=count_propositions(prev_state, l_prev_state, prop_pos);
				}
				if (comp1==comp2) min2=MY_INFINITY*2;
				else min2=delta0_sg(gamma_state, gs_len, goal, g);
				if (min2<min){min=min2; min_opt=options_pos[j];}
				//printf("not segmentated after delta j=%d\n", j);
			}
						//update prev_state
			printf("minimum action is: %d, with delta value of %d\n", options[min_opt], min);
			for(x=1;x<=i;x++)
			{
				gamma_state[x]=state[x];
				prev_state[x]=state[x];
			}
			l_prev_state=i;
			gs_len=i;
			gs_len=execute_action(gamma_state, gs_len, options, min_opt);
			//get the new plan variable
			uint8_t new_plan[250];
			for(x=1;x<=pi;x++)
			{
				new_plan[x]=plan[x];
			}
			int new_pi=pi;
			//add action to new plan
			new_pi=add_action(new_plan, new_pi, options, min_opt);
			//printf("new_plan is:\n");
			//print_actions(new_plan, new_pi);
			//delete_action
			opt_len=delete_action(options, opt_len, min_opt);
			//printf("Remaining options are:\n");
			//print_actions(options, opt_len);

			new_pi = forward_search(new_plan, new_pi, gamma_state, gs_len, goal, g, prev_state, l_prev_state);
			if (new_pi!=0)
			{
				for(x=1;x<=new_pi;x++)
				{
					plan[x]=new_plan[x];
				}
				pi=new_pi;
				return new_pi;
			}
		}
		return 0;
	}
}
