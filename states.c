#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "utils.h"
#include "actions.h"
#include "basicf.h"
#include "exercise4f.h"
#include "exercise5f.h"
#include "exercise6f.h"

void main()
{
	//now prepare the variable for the state
	uint8_t state[200], actions[250], goal[200];
	int i=1; int g=1,a;
	int length,x;
	//now use functions to add nummbers to the state vector
	i=attached(state,p1,loc1,i);
	i=in(state,c1,p1,i);
	i=in(state,c3,p1,i);
	i=top(state,c3,p1,i);
	i=on(state,c3,c1,i);
	i=on(state,c1,p1,i);
	i=attached(state,p2,loc1,i);
	i=in(state,c2,p2,i);
	i=top(state,c2,p2,i);
	i=on(state,c2,p2,i);
	i=belong(state,crane1,loc1,i);
	i=empty(state,crane1,i);
	i=adjacent(state,loc1,loc2,i);
	i=adjacent(state,loc2,loc1,i);
	i=at(state,r1,loc2,i);
	i=occupied(state,loc2,i);
	i=unloaded(state,r1,i);
	print_state(state,i);
	g=occupied(goal, loc1,g);
	g=at(goal,r1, loc1,g);
	g= loaded(goal,r1, c2, g);
	printf("Goal State is:\n");
	print_state(goal,g);
	int act1_rel= relevance(state, i, goal, g, actions, 1);
	int act2_rel = relevance(state, i, goal, g, actions, 7);
	printf("\nrelevance of action take is: %d\nrelevance of action move is: %d\n", act1_rel, act2_rel);
	//int delta0_sg_value=delta0_sg(state, i, goal, g);
	//printf("\nthe value of delta0_sg is: %d\n", delta0_sg_value); 
	uint8_t plan[250], prev_state[200];
	int pi=1;
	pi=forward_search(plan, pi, state, i, goal, g, prev_state, 0);
	printf("Plan to Goal is:\n");
	print_actions(plan, pi);
	return;
}
