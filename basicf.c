#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "utils.h"
#include "basicf.h"
//---Declaration of the atoms --------------------------------
int adjacent(uint8_t state[200], uint8_t arg1, uint8_t arg2, int i)
{
	state[i]=10; state[i+1]=arg1; state[i+2]=arg2;
	i=i+3;
	return i;
}
int attached(uint8_t state[200], uint8_t arg1, uint8_t arg2, int i)
{
	state[i]=11; state[i+1]=arg1; state[i+2]=arg2;
	i=i+3;
	return i;
}
int belong(uint8_t state[200], uint8_t arg1, uint8_t arg2, int i)
{
	state[i]=12; state[i+1]=arg1; state[i+2]=arg2;
	i=i+3;
	return i;
}
int occupied(uint8_t state[200], uint8_t arg1, int i)
{
	state[i]=13; state[i+1]=arg1;
	i=i+2;
	return i;
}
int at(uint8_t state[200], uint8_t arg1, uint8_t arg2, int i)
{
	state[i]=14; state[i+1]=arg1; state[i+2]=arg2;
	i=i+3;
	return i;
}
int loaded(uint8_t state[200], uint8_t arg1,uint8_t arg2, int i)
{
	state[i]=15; state[i+1]=arg1; state[i+2]=arg2;
	i=i+3;
	return i;
}
int unloaded(uint8_t state[200], uint8_t arg1, int i)
{
	state[i]=16; state[i+1]=arg1;
	i=i+2;
	return i;
}
int holding(uint8_t state[200], uint8_t arg1, uint8_t arg2, int i)
{
	state[i]=17; state[i+1]=arg1; state[i+2]=arg2;
	i=i+3;
	return i;
}
int empty(uint8_t state[200], uint8_t arg1, int i)
{
	state[i]=18; state[i+1]=arg1;
	i=i+2;
	return i;
}
int in(uint8_t state[200], uint8_t arg1, uint8_t arg2, int i)
{
	state[i]=19; state[i+1]=arg1; state[i+2]=arg2;
	i=i+3;
	return i;
}
int on(uint8_t state[200], uint8_t arg1, uint8_t arg2, int i)
{
	state[i]=20; state[i+1]=arg1; state[i+2]=arg2;
	i=i+3;
	return i;
}
int top(uint8_t state[200], uint8_t arg1, uint8_t arg2, int i)
{
	state[i]=21; state[i+1]=arg1; state[i+2]=arg2;
	i=i+3;
	return i;
}
void print_state(uint8_t state[200], int i)
{
	printf("state is:\n");
	//function to print a given state
	int j=1;
	for (j=1;j<=i;j++)
	{
		switch(state[j])
		{
			case p1://p1
				printf("p1 ");
				break;
			case p2://p2
				printf("p2 ");
				break;
			case c1://c1
				printf("c1 ");
				break;
			case c2://c2
				printf("c2 ");
				break;
			case c3://c3
				printf("c3 ");
				break;
			case crane1://crane1
				printf("crane1 ");
				break;
			case r1://r1
				printf("r1 ");
				break;
			case loc1://loc1
				printf("loc1 ");
				break;
			case loc2://loc2
				printf("loc2 ");
				break;
			case adjacent_v://adjacent
				if(j!=1) printf(") ");
				printf("adjacent(");
				break;
			case attached_v://attached
				if(j!=1) printf(") ");
				printf("attached(");
				break;
			case belong_v://belong
				if(j!=1) printf(") ");
				printf("belong(");
				break;
			case occupied_v://occupied
				if(j!=1) printf(") ");
				printf("occupied(");
				break;
			case at_v://at
				if(j!=1) printf(") ");
				printf("at(");
				break;
			case loaded_v://loaded
				if(j!=1) printf(") ");
				printf("loaded(");
				break;
			case unloaded_v://unloaded
				if(j!=1) printf(") ");
				printf("unloaded(");
				break;
			case holding_v://holding
				if(j!=1) printf(") ");
				printf("holding(");
				break;
			case empty_v://empty
				if(j!=1) printf(") ");
				printf("empty(");
				break;
			case in_v://in
				if(j!=1) printf(") ");
				printf("in(");
				break;
			case on_v://on
				if(j!=1) printf(") ");
				printf("on(");
				break;
			case top_v://top
				if(j!=1) printf(") ");
				printf("top(");
				break;
		}
	}
	printf(")\n");
}
////Exercise 3: Actions //////////////////////////////////
//Function to print the actions array
void print_actions(uint8_t actions[250],int i_a)
{
	printf("actions are:\n");
	int j=1;
	for(j=1;j<=i_a;j++)
	{
		switch(actions[j])
		{
			case p1://p1
				printf("p1 ");
				break;
			case p2://p2
				printf("p2 ");
				break;
			case c1://c1
				printf("c1 ");
				break;
			case c2://c2
				printf("c2 ");
				break;
			case c3://c3
				printf("c3 ");
				break;
			case crane1://crane1
				printf("crane1 ");
				break;
			case r1://r1
				printf("r1 ");
				break;
			case loc1://loc1
				printf("loc1 ");
				break;
			case loc2://loc2
				printf("loc2 ");
				break;
			case take_n:
				if(j!=1) printf(") ");
				printf("take(");
				break;
			case put_n:
				if(j!=1) printf(") ");
				printf("put(");
				break;
			case move_n:
				if(j!=1) printf(") ");
				printf("move(");
				break;
			case load_n:
				if(j!=1) printf(") ");
				printf("load(");
				break;
			case unload_n:
				if(j!=1) printf(") ");
				printf("unload(");
				break;
		}
	}
	printf(")\n");
}
//function to look if the precondition is fullfilled
int precond(uint8_t state[200], int i, uint8_t atom, uint8_t arg1, uint8_t arg2)
{
	int j=1;
	for(j=1;j<=i;j++)
	{
		if(state[j]==atom && state[j+1]==arg1)
		{
			if(arg2==0) return j;
			else
			{
				if(state[j+2]==arg2) return j;
			}
		}
	}
	return 0;
}
//function to delete information from the state vector
int delete_eff(uint8_t state[200], int i, uint8_t atom, uint8_t arg1, uint8_t arg2)
{
	int j=1; int n=0;int k;
	//search the element to delete
	for(j=1;j<=i;j++)
	{
		if(state[j]==atom && state[j+1]==arg1)
		{
			if(arg2==0)
			{
				k=j;n=2;j=i;
			}
			else
			{
				if(state[j+2]==arg2)
				{
					k=j;n=3;j=i;
				}
			}
		}
	}
	//if the element to delete is found, delete it
	if(n==2||n==3)
	{
		for(j=k+n;j<=i;j++)
		{
			state[j-n]=state[j];
		}
	}
	i=i-n;
	return i;
}

//function to delete an action from a list of actions
int delete_action(uint8_t actions[250], int a, int a_pos)
{
	int j;
	int sizeofaction= getsizeofaction(actions[a_pos]);
	for(j=a_pos+sizeofaction;j<=a;j++)
	{
		actions[j-sizeofaction]=actions[j];
	}
	return a-sizeofaction;
	
}
//write action in the action vector
int write(uint8_t arg1,uint8_t arg2,uint8_t arg3,uint8_t arg4,uint8_t arg5,uint8_t arg6,uint8_t actions[250],int i_a)
{
	int j=1;
	//check if action is already in the list
	for(j=1;j<=i_a;j++)
	{
		//if the args are already in the list return
		if(actions[j]==arg1 && actions[j+1]==arg2)
		{
			//if in this place, argn==0, there are no more args. Action is already writen. Return 
			if(arg3==0)
			{
				return i_a;
			}
			if(actions[j+2]==arg3)
			{
				if(arg4==0)
				{
					return i_a;
				}
				if(actions[j+3]==arg4)
				{
					if(arg5==0)
					{
						return i_a;
					}
					if(actions[j+4]==arg5)
					{
						if(arg6==0)
						{
							return i_a;
						}
						if(actions[j+5]==arg6)
						{
							return i_a;
						}
					}
				}
			}
		}
	}
	//If the whole list is read and action can not be found, write it.
	actions[i_a]=arg1; i_a++;
	if(arg2!=0){actions[i_a]=arg2; i_a++;}
	if(arg3!=0){actions[i_a]=arg3; i_a++;}
	if(arg4!=0){actions[i_a]=arg4; i_a++;}
	if(arg5!=0){actions[i_a]=arg5; i_a++;}
	if(arg6!=0){actions[i_a]=arg6; i_a++;}
	return i_a;
}

//function to count the number of propositions
int count_propositions(uint8_t state[200], int i, int prop_pos[100])
{
	int j, count=0;
	for(j=1;j<=i;j++)
	{
		if (state[j]>=10 && state[j]<=21)
		{
			count++;
			prop_pos[count]=j;
		}
	}
	return count;
}
int count_actions(uint8_t actions[250], int a, int action_pos[100])
{
	int j, count=0;
	for(j=1;j<=a;j++)
	{
		if (actions[j]>=22 && actions[j]<=26)
		{
			count++;
			action_pos[count]=j;
		}
	}
	return count;
}
int isatom(uint8_t atom)
{
	if(atom>=10 && atom <=21) return 1;
	else return 0;
}
int getsizeofvariable(uint8_t atom)
{
	if (atom == adjacent_v || atom== attached_v || atom == belong_v || atom == at_v || atom == loaded_v || atom == holding_v || atom == in_v || atom == on_v || atom == top_v)
		{
			//if the statement is one of those, their size is 3
			return 3;
		}
		else
		{
			if(atom == occupied_v || atom == unloaded_v || atom == empty_v)
			{
				//if the statement is one of those, their size is 2
				return 2;
			}
		}
}
int getsizeofaction(uint8_t action)
{
	if(action == take_n || action == put_n) return 6;
	else if (action == move_n) return 4;
	else if (action == load_n || action == unload_n) return 5;
	else printf("ACTION NUMBER IS WRONG\n");
}
