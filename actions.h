int precond_list(uint8_t actions[250], int a_pos, uint8_t pre_list[200]);
int take(uint8_t k,uint8_t l,uint8_t c,uint8_t d,uint8_t p,uint8_t state[200], int i, bool j);
int put(uint8_t k,uint8_t l,uint8_t c,uint8_t d,uint8_t p,uint8_t state[200], int i, bool j);
int move(uint8_t r,uint8_t l,uint8_t m,uint8_t state[200], int i, bool j);
int load(uint8_t k,uint8_t l,uint8_t c,uint8_t r,uint8_t state[200], int i, bool j);
int unload(uint8_t k,uint8_t l,uint8_t c,uint8_t r,uint8_t state[200], int i, bool j);
