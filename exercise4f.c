#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "exercise4f.h"
#include "utils.h"
#include "basicf.h"
#include "actions.h"

//Exercise 4: Applicable Actions/////////////////////////
int applicableactions(uint8_t state[200],int i,uint8_t actions[250])
{
	uint8_t k, l, c, p, r, d, m;
	int applicable, a=1, formateo;
	for(formateo=0;formateo<250;formateo++)
	{
		actions[formateo]=0;
	}
	for(k=crane1;k<crane1+NCRANE;k++)
	{
		for(l=loc1;l<loc1+NLOC;l++)
		{
			for(c=c1;c<c1+NC;c++)
			{
				for(p=p1;p<p1+NP;p++)
				{
					for(r=r1;r<r1+NR;r++)
					{
						applicable=load(k,l,c,r,state,i,0);
						if(applicable==1) a=write(load_n, k,l,c,r,0,actions,a);
						applicable=unload(k,l,c,r,state,i,0);
						if(applicable==1) a=write(unload_n, k,l,c,r,0,actions,a);				
						for(d=p1;d<p1+NC+NP;d++)
						{
							applicable=put(k,l,c,d,p,state,i,0);
							if(applicable==1) a=write(put_n, k,l,c,d,p,actions,a);
							applicable=take(k,l,c,d,p,state,i,0);
							//printf("k=%d, l=%d, c=%d, d=%d, p=%d \n",k,l,c,d,p);
							if(applicable==1) a=write(take_n, k,l,c,d,p,actions,a);
							}//end for d
						for(m=loc1;m<loc1+NLOC;m++)
						{
							applicable=move(r,l,m,state,i,0);
							if(applicable==1) a=write(move_n, r,l,m,0,0,actions,a);
						}
					}//end for r
				}//end for p
			}//end for c
		}//end for l
	}//end for k
	return a;
}
int compare_states(uint8_t state1[200],int counts1,uint8_t state2[200],int counts2)
{
	int p_i, search, sizeofvariable,j,k,simil=0;
	for(p_i=1;p_i<=counts2;p_i++)//state2 will be the goal state
	{
		search=isatom(state2[p_i]);
		//go through the initial state to check coincidence with goal
		if(search==1)
		{
			sizeofvariable=getsizeofvariable(state2[p_i]);
			for(j=1;j<=counts1;j++)
			{
				if (sizeofvariable==2)//for example occupied(loc2)
				{
					if (state1[j]==state2[p_i] && state1[j+1]==state2[p_i+1])
					{
						//increase simil
						simil++;
						j=counts1;
					}
				}
				else
				{
					if (sizeofvariable ==3)
					{
						if (state1[j]==state2[p_i] && state1[j+1]==state2[p_i+1] && state1[j+2]==state2[p_i+2])
						{
							//increase simil
							simil++;
							j=counts1;
						}
					}
				}
			}
		}
	}
	return simil;
}
//Excercise 4 relevance of an action////
int relevance(uint8_t state[200],int i,uint8_t goal[200],int g,uint8_t action[250], int a_pos)
{
	//int length = NELEMS(state),x;
	//printf("length of state is :%d\n", length);
	uint8_t i_state[200], new_state[200];
	int x;
	for(x=1;x<=i;x++)
	{
		//declare the initial state
		i_state[x]=state[x];
		//declare the new_state
		new_state[x]=state[x];
	}
	int new_i = i;
	//declare the state after the action to check
	switch(action[a_pos])
	{
		case take_n:
			new_i=take(action[a_pos+1], action[a_pos+2],action[a_pos+3], action[a_pos+4],action[a_pos+5], new_state, new_i, 1);  
			break;
		case put_n:
			new_i=put(action[a_pos+1], action[a_pos+2],action[a_pos+3], action[a_pos+4],action[a_pos+5], new_state, new_i, 1);
			break;
		case move_n:
			new_i=move(action[a_pos+1], action[a_pos+2], action[a_pos+3], new_state, new_i, 1);
			break;
		case load_n:
			new_i= load(action[a_pos+1], action[a_pos+2],action[a_pos+3], action[a_pos+4], new_state, new_i, 1);
			break;
		case unload_n:
			new_i= unload(action[a_pos+1], action[a_pos+2],action[a_pos+3], action[a_pos+4], new_state, new_i, 1);
			break;
	}
	//Compare initial state with goal state --> IG
	int IG=compare_states(i_state, i, goal, g);
	//compare final state with goal state   --> FG
	int FG=compare_states(new_state, new_i, goal, g);
	int Relevance= FG-IG;
	return Relevance;
}
	/*
	#define adjacent_v 10
#define attached_v 11
#define belong_v 12
#define occupied_v 13--
#define at_v 14
#define loaded_v 15
#define unloaded_v 16--
#define holding_v 17
#define empty_v 18--
#define in_v 19
#define on_v 20
#define top_v 21*/
//End of Exercise 4 relevance////
