#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "exercise4f.h"
#include "utils.h"
#include "basicf.h"
#include "actions.h"
int delta0_sg(uint8_t state[200], int i, uint8_t goal[200], int g);

int getproposition(uint8_t goal[200], uint8_t proposition[5], int prop_pos)
{
	int sizeofvariable= getsizeofvariable(goal[prop_pos]);
	proposition[1]=goal[prop_pos];
	if (sizeofvariable==2)
	{
		proposition[2]=goal[prop_pos+1];
		proposition[3]=0;
		proposition[4]=0;
	}
	else
	{
		if (sizeofvariable==3)
		{
			proposition[2]=goal[prop_pos+1];
			proposition[3]=goal[prop_pos+2];
			proposition[4]=0;
		}
	}
	//printf("Proposition is:\n");
	//print_state(proposition, sizeofvariable);
	return sizeofvariable;
}
int delta0_sp(uint8_t state[200], int i, uint8_t proposition[5], int sizeofvariable)
{
	//printf("\n\ndelta0_SP info:\n");
	//print_state(state, i);
	//first save in the proposition variable the proposition p
	uint8_t actions[250];//,proposition[5];
	int actions_pos[100],j;
	int isprop=compare_states(state, i,proposition, sizeofvariable);
	//printf("not failed after compare_states SP\n");
	//printf("isprop value: %d\n", isprop);
	int iseffect =0, efflag=0;
	//if it is: return 0
	if(isprop==1)	return 0;
	else
	{
		//if it is not in the state: check applicable actions
		int a=applicableactions(state,i,actions);
		//printf("Applicable actions are:\n");
		//print_actions(actions, a);
		//printf("not failed before count_actions SP\n");
		int n_a=count_actions(actions,a,actions_pos);
		//printf("not failed after count_actions SP\n");
		//printf("number of actions is: %d\n", n_a);
		int n_g_a=0; //number of good actions
		int good_actions_pos[100];//position in actions array of good actions
		for(j=1;j<=n_a;j++)
		{
			//check if p can be reached with the applicable actions
			//printf("not failed before relevance SP\n");
			iseffect= relevance(state, i, proposition, sizeofvariable, actions, actions_pos[j]);
			//printf("not failed after relevance SP\n");
			//printf("iseffect value is:%d action is:%d\n",iseffect, actions[actions_pos[j]]);
			//if p can be reached with an action save it in a list of possible actions
			if(iseffect>0)
			{
				efflag=1;
				n_g_a++;
				good_actions_pos[n_g_a]=actions_pos[j];
			}
			
		}
		if(efflag==0) return MY_INFINITY;
		else
		{
			//if p can be reached get minimum path to preconditions of good actions
			uint8_t pre_list[200];
			//printf("not failed before precond_list SP\n");
			int l_pre_list = precond_list(actions, actions_pos[1],pre_list);
			//printf("preconditions of action %d are:\n", actions[actions_pos[1]]);
			//print_state(pre_list, l_pre_list);
			//printf("not failed after precond_list SP\nalso before call sg SP\n");
			int min;
			int segmentation=compare_states(pre_list, l_pre_list, proposition, sizeofvariable);
			if(segmentation==0) min=1+delta0_sg(state, i, pre_list,l_pre_list);
			else min=MY_INFINITY;
			//printf("not failed after calling sg SP\n");
			int min2;
			//printf("not failed before loop precond to sg SP\n");
			for(j=2;j<=n_g_a;j++)
			{
				l_pre_list = precond_list(actions, actions_pos[j],pre_list);
				segmentation=compare_states(pre_list, l_pre_list, proposition, sizeofvariable);
				if(segmentation==0) min2= 1+delta0_sg(state, i, pre_list,l_pre_list);
				else min2=MY_INFINITY;
				if (min2<min) min=min2;
			}
			//printf("not failed after loop precond to sg SP\n");
			return min;
		}
	}
}
int delta0_sg(uint8_t state[200], int i, uint8_t goal[200], int g)
{
	//printf("\n\nInfo of delta0_SG:\n");
	//print_state(state, i);
	//print_state(goal,g);
	uint8_t proposition[5];
	int prop_pos[100], sum=0,j, sizeofvariable; //this variable will contain the addresses of the proposition of the goal
	int a=compare_states(state, i, goal, g);
	int b=count_propositions(goal, g, prop_pos);
	//printf("is the goal reached? a=%d, b=%d\n", a, b);
	if (a==b) return 0;
	else
	{
		int delta0_value;
		for(j=1; j<=b;j++)
		{
			sizeofvariable= getproposition(goal, proposition, prop_pos[j]);
			delta0_value=delta0_sp(state, i ,proposition, sizeofvariable);
			//if (delta0_value==MY_INFINITY) return MY_INFINITY;
			sum=sum+delta0_value;//else sum=sum+delta0_value;
		}
		return sum;
	}
}
